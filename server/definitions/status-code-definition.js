
const SUCCESS_STATUS_CODE = 200;
const UNAUTHORIZED_STATUS_CODE = 401;
const SERVER_ERROR_STATUS_CODE = 500;

module.exports = {
    SUCCESS_STATUS_CODE,
    UNAUTHORIZED_STATUS_CODE,
    SERVER_ERROR_STATUS_CODE
}
