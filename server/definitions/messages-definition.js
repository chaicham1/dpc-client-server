/* Controllers */
const SUCCESS_MESSAGE = 'Operation succeeded';
const UNAUTHORIZED_MESSAGE = 'Auth failed';
const AUTH_SUCCEEDED_MESSAGE = 'Auth succeeded';
const NO_PERMISSION_MESSAGE = 'No permission';
const NOT_LEGAL_USER_ROLE_MESSAGE = 'Not a legal user role';
const OPERATION_FAILED_MESSAGE = 'Operation failed';
const LOGIN_SUCCESSFUL_MESSAGE = 'Login successful';
const UPDATE_FAILED_MESSAGE = 'Update has been failed';
const UPDATE_SUCCEEDED_MESSAGE = 'Update has been succeeded';
const SUCCESSFULLY_WORKED_MESSAGE = 'Successfully worked';

/* DB */


module.exports ={
    SUCCESS_MESSAGE,
    UNAUTHORIZED_MESSAGE,
    AUTH_SUCCEEDED_MESSAGE,
    NO_PERMISSION_MESSAGE,
    NOT_LEGAL_USER_ROLE_MESSAGE,
    OPERATION_FAILED_MESSAGE,
    LOGIN_SUCCESSFUL_MESSAGE,
    UPDATE_FAILED_MESSAGE,
    UPDATE_FAILED_MESSAGE,
    UPDATE_SUCCEEDED_MESSAGE,
    SUCCESSFULLY_WORKED_MESSAGE
}
